% Game Development with ![Vue.js](img/vue-logo.png) / ![Nuxt.js](img/nuxt-logo.svg) and ![Phaser](img/phaser-logo.gif)
% [Winnie Hellmann](https://gitlab.com/winh)
% 2019-10-25

## Frontend Engineer

[![GitLab Logo](img/gitlab-logo.svg){style="height: 50vh;"}](https://about.gitlab.com/)

## Whack-a-Pipeline {data-background-video="img/whack-a-pipeline.mp4" data-background-video-loop=true data-background-size="contain"}

## {#entire-devops-cycle data-background-image="img/devops-cycle.png" data-background-size="90%"}

# Why?

…would you do that?

![ponfused carrot](img/ponfused-carrot.png){style="height: 7em; padding: 1em;"}

<small>picture by [Nathan Friend](https://nathanfriend.io/)</small>

## Why browser games? 🚿

- click and play
  - good for game jams
  - good to share with friends
- powerful platform
  - text rendering and alignment
  - pre-defined UI components
  - asset loading and caching built-in
- developer tools
- simple update mechanism

## Why [Phaser](https://phaser.io/)? ![](img/phaser-logo.gif)

- MIT License
- parsers and renderers for
  - tiled maps
  - animated sprites
- physics engines
- particle emitters
- 2D / 3D cameras
- tweens
- very detailed documentation
- big collection of examples

## Why [Vue.js](https://vuejs.org/)? ![](img/vue-logo.png){style="height: 0.8em; padding: 0.1em;"}

- collocate HTML, CSS, JavaScript with [Single File Components](https://vuejs.org/v2/guide/single-file-components.html)
- extract / reuse components such as menus, modals, or overlays
- [Vue Router](https://router.vuejs.org/) allows navigation between main menu, game, settings, …
- [reactivity](https://vuejs.org/v2/guide/reactivity.html) allows to synchronize different components of the game

## Why [Vuex](https://vuex.vuejs.org/)? ![](img/vue-logo.png){style="height: 0.8em; padding: 0.1em;"}

- official state management solution for Vue.js
- decouples components
- allows global application state
- makes business logic testable without rendering

# What?

…can you do?

![giant rubber duck](https://florentijnhofman.nl/sites/default/files/styles/frontpage-slick/public/home/_7ku9593.jpg?itok=UkOA-agh){style="height: 9em;"}

<small>picture by [Florentijn Hofman](https://florentijnhofman.nl)</small>

## Game entities in store

- actions to initialize and access entities
- keep entities in local variables of store modules

- **Disadvantages:**
  - each exposed Phaser method requires an action
  - debug tools cannot easily access entities
  - strong coupling of Phaser and store

## Reactive game entities

- overwrite properties of game entities with (derived) store state
- actions/mutations update store
- Phaser rendering picks up changes

- **Disadvantages:**
  - only works for properties
  - breaks if Phaser overwrites property
  - property changes may cause performance problems

## Store watchers

- game entities register watchers for relevant store state

- **Disadvantages:**
  - potentially more verbose
  - difficult to find all code that depends on certain state
  - not possible to track events that don't mutate state

## Subscribe action

- listener for dispatched store actions
- triggers even if state remains same

- **Disadvantages:**
  - hard to track when following data flow

# How?

…can you do it?

![drawing of recipes](img/recipes.png){style="height: 9em;"}

<small>picture by [dasMaichen](https://dasMaichen.de)</small>

# Y<span style="margin-left: -0.1em"></span>Yhy?

…is the carrot back?

![ponfused carrot](img/ponfused-carrot.png){style="height: 7em; padding: 1em;"}

<small>picture by [Nathan Friend](https://nathanfriend.io/)</small>

## Wrapper components?

- Vue follows the [One-Way Data Flow](https://vuejs.org/v2/guide/components-props.html#One-Way-Data-Flow)  
  &rArr; data to child components via reactive props
- Phaser objects are doubly linked  
  &rArr; Vue fails to recurse them
- some Phaser objects are updated often
  <small>(physics, tweens)</small>  
  &rArr; performance problems
- `Object.freeze()` prevents reactivity  
  …but also updating properties such as position

## Wrapper components?

- Vue components need single root DOM element  
  …but Phaser renders to canvas
- workaround:  
  create HTML comment using render function  
  …but that does not allow child components
- workaround:  
  a lot of `<div>`s

## All games?

- avoid store actions for data that changes often <small>(for example position with physics)</small>  
- possible workarounds:
  - debounce
  - tile-based movement
  - collision events

- good fit: Adventure, Point And Click, Puzzle
- still ok: Jump 'n' Run
- bad fit: First-person shooter, Racing

## Links

- real world examples:
  - <https://ldjam.com/events/ludum-dare/45/zwsp>
  - <https://ldjam.com/events/ludum-dare/44/between-light-and-shadow>
- slides: <https://gitlab.com/winnie-slides/2019/nuxt-phaser-games-en>
- examples: <https://gitlab.com/winnie-slides/2019/nuxt-phaser-games-examples>

# Thanks for listening! <br> <span style="display: inline-block; transform: scale(-1, 1);">👂</span>👀👂 <br> 🍿
