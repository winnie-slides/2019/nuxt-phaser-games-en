#!/usr/bin/env sh

pandoc \
  --standalone \
  --mathjax \
  --to=revealjs \
  --variable=theme:white \
  --variable=backgroundTransition:none \
  --css=style.css \
  --output=index.html \
  --slide-level=2 \
  --incremental \
  -V autoPlayMedia=true \
  slides.md
